﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.CommonClass
{
    public class Response<T>
    {
        public T ResponseClass{get;set;}
        public ResponseCode Status{ get; set; }
        public string Message { get { return Status.Display(); } }
    }
}
