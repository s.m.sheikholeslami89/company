﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.CommonClass
{
    public interface IBaseInterface<TEntity>
    {

        Response<IEnumerable<TEntity>> GetAll();
        Response<TEntity> GetById(int id);
        Response<bool> Add(TEntity entity);
        Response<bool> Update(TEntity entity);
        Response<bool> Delete(int id);
    }
}
