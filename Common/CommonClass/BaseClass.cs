﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common.CommonClass
{
    /// <summary>
    /// base class of all entities
    /// </summary>
    public abstract class BaseClass
    {
        protected BaseClass()
        {
            CreatedOn = DateTime.Now;
        }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
