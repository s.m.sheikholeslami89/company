﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.CommonClass;
using Common.Entities;
using Common.Infra;
using Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Common.Services
{
    public class OwnerService: IOwnerService
    {
        private readonly DbContext _context;

        public OwnerService(CommonContext context)
        {
            _context = context;
        }
        public Response<IEnumerable<Owner>> GetAll()
        {
            try
            {
                return new Response<IEnumerable<Owner>>()
                {
                    ResponseClass = _context.Set<Owner>(),
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<IEnumerable<Owner>>()
                {
                    ResponseClass = null,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<Owner> GetById(int id)
        {
            try
            {
                return new Response<Owner>()
                {
                    ResponseClass = _context.Set<Owner>().Find(id),
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<Owner>()
                {
                    ResponseClass = null,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<bool> Add(Owner entity)
        {
            try
            {
                _context.Set<Owner>().Add(entity);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<bool> Update(Owner entity)
        {
            try
            {
                entity.UpdatedOn = DateTime.Now;
                _context.Set<Owner>().Update(entity);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail,
                };
            }
        }

        public Response<bool> Delete(int id)
        {
            try
            {
                var entity = GetById(id).ResponseClass;
                _context.Set<Owner>().Remove(entity);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<Owner> GetOwnersByName(string name)
        {
            try
            {
                return new Response<Owner>()
                {
                    ResponseClass = _context.Set<Owner>().FirstOrDefault(o=> o.Name==name),
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<Owner>()
                {
                    ResponseClass = null,
                    Status = ResponseCode.Fail
                };
            }
        }
    }
}
