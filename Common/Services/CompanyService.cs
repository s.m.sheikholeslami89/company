﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.CommonClass;
using Common.Entities;
using Common.Infra;
using Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Common.Services
{
    public class CompanyService: ICompanyService
    {
        private readonly IOwnerService _ownerService;
        private readonly DbContext _context;

        public CompanyService(CommonContext context,IOwnerService ownerService)
        {
            _context = context;
            _ownerService = ownerService;
        }
        public Response<IEnumerable<Company>> GetAll()
        {
            try
            {
                return new Response<IEnumerable<Company>>()
                {
                    ResponseClass = _context.Set<Company>().Include(o=>o.Owners),
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<IEnumerable<Company>>()
                {
                    ResponseClass = null,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<Company> GetById(int id)
        {
            try
            {
                return new Response<Company>()
                {
                    ResponseClass = _context.Set<Company>().Include(o=>o.Owners).FirstOrDefault(o=>o.CompanyId==id),
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<Company>()
                {
                    ResponseClass = null,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<bool> Add(Company entity)
        {
            try
            {
                _context.Set<Company>().Add(entity);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<bool> Update(Company company)
        {
            try
            {
                company.UpdatedOn = DateTime.Now;
                _context.Set<Company>().Update(company);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail,
                };
            }
        }

        public Response<bool> Delete(int id)
        {
            try
            {
                var entity = GetById(id).ResponseClass;
                _context.Set<Company>().Remove(entity);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail
                };
            }
        }

        public Response<bool> AddOwner(int companyId,Owner owner)
        {
             try
            {
                Response<Company> findCompany = GetById(companyId);
                if(findCompany.Status==ResponseCode.Fail)
                {
                    return new Response<bool>()
                    {
                        ResponseClass = false,
                        Status = ResponseCode.Fail
                    };
                }
                //var findOwner=_ownerService.GetOwnersByName(owner.Name);
                //if(findOwner.ResponseClass==null)
                //{
                //    _ownerService.Add(owner);
                //}
                (findCompany.ResponseClass.Owners??= new List<Owner>()).Add(owner);
                _context.Set<Company>().Update(findCompany.ResponseClass);
                _context.SaveChanges();
                return new Response<bool>()
                {
                    ResponseClass = true,
                    Status = ResponseCode.Success
                };
            }
            catch (Exception e)
            {
                return new Response<bool>()
                {
                    ResponseClass = false,
                    Status = ResponseCode.Fail
                };
            }
        }
    }
}
