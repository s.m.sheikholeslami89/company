﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.CommonClass;
using Common.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace Common.Infra.Mapping
{
    public static class CustomMapping
    {
        public static void RegisterAll(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new OwnerConfiguration());
        }
        public class CompanyConfiguration : IEntityTypeConfiguration<Company>
        {
            public void Configure(EntityTypeBuilder<Company> builder)
            {
                builder.HasKey(o => o.CompanyId);
                builder.Property(o =>o.Name).HasMaxLength(32).IsRequired();
                builder.Property(o =>o.Address).HasMaxLength(512).IsRequired();
                builder.Property(o =>o.City).HasMaxLength(32).IsRequired();
                builder.Property(o =>o.Country).HasMaxLength(32).IsRequired();
                builder.Property(o =>o.Email).HasMaxLength(64);
                builder.Property(o => o.Phone).HasMaxLength(12);//.IsFixedLength();
                builder.HasMany(o => o.Owners);
            }
        }
        public class OwnerConfiguration : IEntityTypeConfiguration<Owner>
        {
            public void Configure(EntityTypeBuilder<Owner> builder)
            {
                builder.HasKey(o => o.OwnerId);
                builder.Property(o =>o.Name).HasMaxLength(32).IsRequired();

            }
        }
        
        
    }

    
}
