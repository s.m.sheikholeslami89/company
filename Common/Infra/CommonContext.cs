﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Entities;
using Common.Infra.Mapping;
using Microsoft.EntityFrameworkCore;

namespace Common.Infra
{
    public class CommonContext: DbContext
    {
        public CommonContext(DbContextOptions<CommonContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            CustomMapping.RegisterAll(modelBuilder);
        }
        public DbSet<Company> Companies{ get; set; }
        public DbSet<Owner> Owners{ get; set; }
    }
}
