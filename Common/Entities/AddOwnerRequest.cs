﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class AddOwnerRequest
    {
        public int CompanyId { get; set; }
        public Owner Owner{ get; set; }
    }
}
