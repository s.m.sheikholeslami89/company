using Common.CommonClass;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Entities
{
    public class Owner : BaseClass
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OwnerId { get; set; } 
        public string Name { get; set; }
    }
}
