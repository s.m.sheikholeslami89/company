﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.CommonClass;
using Common.Entities;


namespace Common.Interfaces
{
    public interface ICompanyService : IBaseInterface<Company>
    {
        Response<bool> AddOwner(int companyId,Owner owner);
    }
}
