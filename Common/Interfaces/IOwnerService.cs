﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.CommonClass;
using Common.Entities;


namespace Common.Interfaces
{
    public interface IOwnerService : IBaseInterface<Owner>
    {
        Response<Owner> GetOwnersByName(string name);
    }
}
