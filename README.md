Requirements
-------------

.Net Core 3.1

EF Core 3.1.3

Initializing Database 
----------------------

To create the database, you should go to “webapi\appsetting.json” and change ConnectionStrings to use your server. Then you should update database to build it. After building databases, you can use these curl commands in Postman for using APIs.


Add Company
-----------

       curl --location --request POST 'https://localhost:5001/api/v1/company/AddCompany' \
            --header 'Content-Type: application/json' \
            --header 'Content-Type: text/plain' \
            --data-raw '{
	           "Name":"MaryCo",
               "Address":"behnam",
               "City":"Tehran",
               "Country":"Iran",
               "Email":"dfs@sd.cc",
               "Phone":"+479115474",
               "owners":[{"companyId":3,
			   "Name":"Mehdi"}]
            }'

Add Owner to company
--------------------

      curl --location --request POST 'https://localhost:5001/api/v1/company/AddOwner' \
           --header 'Content-Type: application/json' \
           --header 'Content-Type: text/plain' \
           --data-raw '{
        	  "CompanyId":1,
              "Owner":{"OwnerId":3,"Name":"Mehdi"}
            }'

Get all companies
-----------------

      curl --location --request GET 'https://localhost:5001/api/v1/company/AllCompany'
	 
Get company by Id
-----------------

     curl --location --request GET 'https://localhost:5001/api/v1/company/GetCompany?id=1'

Update
------

     curl --location --request PUT 'https://localhost:5001/api/v1/company/UpdateCompany' \
          --header 'Content-Type: application/json' \
          --data-raw '{
	        "companyId": 2,
            "name": "mehran co",
            "address": "behnam",
            "city": "Tehran",
            "country": "Iran",
            "email": "dfs@sd.cc",
            "phone": "+479115474",
            "createdOn": "2020-03-29T00:26:49.9294606",
            "updatedOn": null
            }'
