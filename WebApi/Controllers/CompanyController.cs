﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Common.CommonClass;
using Common.Entities;

namespace company.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        [Route("AllCompany")]
        public Response<IEnumerable<Company>> GetAllCompanies()
        {
            return _companyService.GetAll();
        }
        [HttpGet]
        [Route("GetCompany")]
        public Response<Company> GetCompanyById(int id)
        {
            return _companyService.GetById(id);
        }
        [HttpPost]
        [Route("AddCompany")]
        public Response<bool> AddCompany([FromBody] Company company)
        {
            return _companyService.Add(company);
        }
        [HttpPost]
        [Route("AddOwner")]
        public Response<bool> AddOwner([FromBody] AddOwnerRequest addOwnerRequest)
        {
            return _companyService.AddOwner(addOwnerRequest.CompanyId,addOwnerRequest.Owner);
        }
        [HttpPut]
        [Route("UpdateCompany")]
        public Response<bool> UpdateComany([FromBody] Company company)
        {
            return _companyService.Update(company);
        }
    }
}
